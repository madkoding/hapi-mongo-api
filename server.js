'use strict';

const Hapi = require('hapi');
const mongojs = require('mongojs');

// Create a server with a host and port
const server = new Hapi.Server();
server.connection({
    host: 'localhost',
    port: 3000
});

//Connect to db
server.app.db = mongojs('aqui va el chorizo de la url', ['Contact']);

//Load plugins and start server
server.register([
    require('./routes/contact')
], (err) => {

    if (err) {
        throw err;
    }

    // Start the server
    server.start(() => {
        console.log('Server running at:', server.info.uri);
    });

});
